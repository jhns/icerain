# IceRain

IceRain is a theme for the Openbox window manager.

## Instructions

Run `make install` to install the theme. <br>
Run `make uninstall` to uninstall it.

[IceRain Screenshot](icerain.png)
